"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = __importDefault(require("node-fetch"));
const md5_1 = __importDefault(require("md5"));
class BusinessRuError extends Error {
    constructor(error_text, payload) {
        super(error_text);
        this.isBusinessRuError = true;
        Object.assign(this, Object.assign(Object.assign({}, payload), { error_text }));
    }
}
const sleep = (time) => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve();
        }, time);
    });
};
const ksort = (map) => {
    const result = Array.from(map);
    result.sort(([a], [b]) => a.charCodeAt(0) - b.charCodeAt(0));
    return new Map(result);
};
const http_build_query = function (queryData) {
    if (!queryData) {
        return '';
    }
    const query = [];
    for (const [key, value] of queryData) {
        let convertedValue = '';
        convertedValue = value === true ? '1' : value;
        convertedValue = value === false ? '0' : value;
        convertedValue = value === 0 ? '0' : value;
        convertedValue = convertedValue || '';
        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(convertedValue));
    }
    return query.join('&').replace(/[!'()*]/g, '');
};
class BusinessRu {
    constructor(app_id, token, address, sleep_on_limit_reached = false) {
        this.SLEEP_INTERVAL = 5 * 60 + 1;
        this.secret = '';
        this.request = (action, model, paramsObject) => __awaiter(this, void 0, void 0, function* () {
            let params = new Map(Object.entries(paramsObject || {}));
            if (params.has('images')) {
                if (Array.isArray(params.get('images'))) {
                    params.set('images', JSON.stringify(params.get('images')));
                }
            }
            params.set('app_id', this.app_id);
            const sortedParams = ksort(params);
            let params_string = http_build_query(sortedParams);
            params = new Map();
            console.log('psw', this.token, this.secret, params_string);
            params.set('app_psw', md5_1.default(this.token + this.secret + params_string));
            params_string += '&' + http_build_query(params);
            const url = 'https://' +
                this.address +
                '.business.ru' +
                '/api/rest/' +
                model +
                '.json';
            const method = action.toUpperCase();
            console.log(method === 'GET' ? `${url}?${params_string}` : url);
            const response = yield node_fetch_1.default(method === 'GET' ? `${url}?${params_string}` : url, {
                method: method,
                body: method === 'GET' ? undefined : params_string,
            });
            const status_code = response.status;
            if (this.isLimitReached(status_code) && this.sleep_on_limit_reached) {
                yield sleep(this.SLEEP_INTERVAL);
                yield this.repair();
                return this.request(action, model, params);
            }
            if (status_code == 200) {
                let responseJson;
                try {
                    responseJson = yield response.json();
                }
                catch (e) {
                    const error_text = `Invalid Response Format (JSON: ${e.message}): "${response.body}"`;
                    throw new BusinessRuError(error_text, {
                        status: 'error',
                        error_code: 'invalid_response_format',
                    });
                }
                const _a = responseJson, { app_psw } = _a, responseData = __rest(_a, ["app_psw"]);
                if (true ||
                    md5_1.default(this.token + this.secret + JSON.stringify(responseData)) ===
                        app_psw) {
                    const { token } = responseData, result = __rest(responseData, ["token"]);
                    this.token = token;
                    return result;
                }
                throw new BusinessRuError('Ошибка авторизации', {
                    status: 'error',
                    error_code: 'auth:1',
                });
            }
            throw new BusinessRuError('', {
                status: 'error',
                error_code: `http:${response.status}`,
            });
        });
        this.repair = () => __awaiter(this, void 0, void 0, function* () {
            let params = new Map([['app_id', this.app_id]]);
            let params_string = http_build_query(params);
            params = new Map([['app_psw', md5_1.default(this.secret + params_string)]]);
            params_string += '&' + http_build_query(params);
            const url = 'https://' + this.address + '.business.ru' + '/api/rest/repair.json';
            const response = yield node_fetch_1.default(`${url}?${params_string}`);
            const status_code = response.status;
            if (status_code === 200) {
                const _b = yield response.json(), { app_psw } = _b, result = __rest(_b, ["app_psw"]);
                if (md5_1.default(this.secret + JSON.stringify(result)) === app_psw) {
                    this.token = result.token;
                    return {
                        status: 'ok',
                        token: result.token,
                    };
                }
                throw new BusinessRuError('Ошибка авторизации', {
                    status: 'error',
                    error_code: 'auth:1',
                });
            }
            throw new BusinessRuError('', {
                status: 'error',
                error_code: `http:${status_code}`,
            });
        });
        this.app_id = app_id;
        this.token = token;
        this.address = address;
        this.sleep_on_limit_reached = sleep_on_limit_reached;
    }
    isLimitReached(status_code) {
        return status_code == 503;
    }
    setSecret(secret) {
        this.secret = secret;
    }
    setToken(token) {
        this.token = token;
    }
    deinstall() {
        throw new BusinessRuError('Deinstall method is not implemented yet. If you want to implement, use the official PHP ' +
            'lib for reference.: https://bitbucket.org/projects_business_ru/business.ru-api-lib-php/src/default/business_ru_api.php', { status: 'error', error_code: null });
    }
}
exports.default = BusinessRu;
//# sourceMappingURL=index.js.map