/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/camelcase */
import fetch from 'node-fetch';
import MD5 from 'md5';

class BusinessRuError extends Error {
  isBusinessRuError = true;

  constructor(
    error_text: string,
    payload: {
      status: string;
      error_code: string;
      [key: string]: unknown;
    },
  ) {
    super(error_text);
    Object.assign(this, { ...payload, error_text });
  }
}

/**
 * @param {number} time - time in milliseconds to sleep
 */
const sleep = (time: number): Promise<void> => {
  return new Promise<void>(resolve => {
    setTimeout(() => {
      resolve();
    }, time);
  });
};

/**
 * Sort map by key;
 */
const ksort = (map: Map<string, any>): Map<string, any> => {
  const result = Array.from(map);
  result.sort(([a], [b]) => a.charCodeAt(0) - b.charCodeAt(0));
  return new Map(result);
};

/**
 * Based on https://github.com/vladzadvorny/http-build-query
 */
const http_build_query = function(queryData: Map<string, any>): string {
  if (!queryData) {
    return '';
  }
  const query: string[] = [];
  for (const [key, value] of queryData) {
    let convertedValue = '';
    convertedValue = value === true ? '1' : value;
    convertedValue = value === false ? '0' : value;
    convertedValue = value === 0 ? '0' : value;
    convertedValue = convertedValue || '';
    query.push(
      encodeURIComponent(key) + '=' + encodeURIComponent(convertedValue),
    );
  }
  return query.join('&').replace(/[!'()*]/g, '');
};

export default class BusinessRu {
  SLEEP_INTERVAL = 5 * 60 + 1;

  private secret = '';

  private app_id: string;
  private token: string;
  private address: string;

  protected sleep_on_limit_reached: boolean;

  constructor(app_id, token, address, sleep_on_limit_reached = false) {
    this.app_id = app_id;
    this.token = token;
    this.address = address;
    this.sleep_on_limit_reached = sleep_on_limit_reached;
  }

  protected isLimitReached(status_code: number): boolean {
    return status_code == 503;
  }

  public setSecret(secret: string): void {
    this.secret = secret;
  }
  public setToken(token: string): void {
    this.token = token;
  }

  // This method is not ported pr
  public deinstall(): void {
    throw new BusinessRuError(
      'Deinstall method is not implemented yet. If you want to implement, use the official PHP ' +
        'lib for reference.: https://bitbucket.org/projects_business_ru/business.ru-api-lib-php/src/default/business_ru_api.php',
      { status: 'error', error_code: null },
    );
  }

  public request = async (
    action: string,
    model: string,
    paramsObject?: Record<string, any>,
  ): Promise<Record<string, any>> => {
    let params = new Map(Object.entries(paramsObject || {}));
    if (params.has('images')) {
      if (Array.isArray(params.get('images'))) {
        params.set('images', JSON.stringify(params.get('images')));
      }
    }
    params.set('app_id', this.app_id);
    const sortedParams = ksort(params);
    let params_string = http_build_query(sortedParams);
    params = new Map<string, string>();
    console.log('psw', this.token, this.secret, params_string);
    params.set('app_psw', MD5(this.token + this.secret + params_string));
    params_string += '&' + http_build_query(params);
    const url =
      'https://' +
      this.address +
      '.business.ru' +
      '/api/rest/' +
      model +
      '.json';
    const method = action.toUpperCase();
    console.log(method === 'GET' ? `${url}?${params_string}` : url);
    const response = await fetch(
      method === 'GET' ? `${url}?${params_string}` : url,
      {
        method: method,
        body: method === 'GET' ? undefined : params_string,
      },
    );
    const status_code = response.status;

    // This is breaking point
    if (this.isLimitReached(status_code) && this.sleep_on_limit_reached) {
      await sleep(this.SLEEP_INTERVAL);
      await this.repair();
      return this.request(action, model, params);
    }

    if (status_code == 200) {
      let responseJson: unknown;
      try {
        responseJson = await response.json();
      } catch (e) {
        const error_text = `Invalid Response Format (JSON: ${e.message}): "${response.body}"`;
        throw new BusinessRuError(error_text, {
          status: 'error',
          error_code: 'invalid_response_format',
        });
      }
      const { app_psw, ...responseData } = responseJson as any;
      if (
        // TODO: implement response authorization
        // eslint-disable-next-line no-constant-condition
        true ||
        MD5(this.token + this.secret + JSON.stringify(responseData)) ===
          app_psw
      ) {
        const { token, ...result } = responseData;
        this.token = token;
        return result;
      }
      throw new BusinessRuError('Ошибка авторизации', {
        status: 'error',
        error_code: 'auth:1',
      });
    }

    throw new BusinessRuError('', {
      status: 'error',
      error_code: `http:${response.status}`,
    });
  };

  public repair = async (): Promise<{ status: string; token: string }> => {
    let params = new Map([['app_id', this.app_id]]);
    let params_string = http_build_query(params);
    params = new Map([['app_psw', MD5(this.secret + params_string)]]);

    params_string += '&' + http_build_query(params);
    const url =
      'https://' + this.address + '.business.ru' + '/api/rest/repair.json';

    const response = await fetch(`${url}?${params_string}`);
    const status_code = response.status;
    if (status_code === 200) {
      const { app_psw, ...result } = await response.json();
      if (MD5(this.secret + JSON.stringify(result)) === app_psw) {
        this.token = result.token;
        return {
          status: 'ok',
          token: result.token,
        };
      }
      throw new BusinessRuError('Ошибка авторизации', {
        status: 'error',
        error_code: 'auth:1',
      });
    }
    throw new BusinessRuError('', {
      status: 'error',
      error_code: `http:${status_code}`,
    });
  };
}
