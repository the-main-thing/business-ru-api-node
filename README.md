# business-ru-api-node

NodeJS вариант [официальной библиотеки](https://developers.business.ru/api-polnoe/biblioteka__business_ru_api_libphp/368) для взаимодействия с API business.ru.
Написана на TypeScript. JS вариант в каталоге /build. Использует Map, object destruction, async, поэтому на старых версиях Node работать не будет. Проверял с 12-й.

Не портирован только метод `.deinstall`, который используется для удаления интеграции. Если вы пишете частную интеграцию, он вам не понадобится.  

Если в ответ от сервера приходит ошибка, приложение бросает ошибку вида:

```typescript
{
  message: 'Error message',
  isBusinessRuError: true, // value is always true
  status: 'error', // value is always 'error'
  error_code: 'some error code',
  error_text: 'Error message' // same as in the .message
}
```

Если хотите помочь, вот список дел:

- Починить авторизацию ответов с сервера в методе `.request`
- Настроить окружение для разработки
- Написать документацию
- Написать тесты
- Собрать npm пакет
- Проверить работоспособность с разными версиями NodeJS
- Портировать `.deinstall` метод
- Добавить более информативные сообщения об ошибках (обязательно с документацией)
- Написать "спасибо"
